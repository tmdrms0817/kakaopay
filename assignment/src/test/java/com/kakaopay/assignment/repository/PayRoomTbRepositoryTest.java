package com.kakaopay.assignment.repository;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PayRoomTbRepositoryTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PayRoomTbRepository payRoomTbRepository;

    @Test
    public void findCountByUserIdAndRoomId(){

        long count = payRoomTbRepository.countByRoomId("abcde");
        logger.debug("count="+count);
    }
}
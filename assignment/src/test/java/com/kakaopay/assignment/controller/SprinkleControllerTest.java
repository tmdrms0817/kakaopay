package com.kakaopay.assignment.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class SprinkleControllerTest extends AbstractControllerTest{

    @Autowired
    private SprinkleController sprinkleController;

    @Override
    protected Object controller() {
        return sprinkleController;
    }

    @Test
    public void postSprinkle() throws Exception{

        // 정상 인 경우
        mockMvc.perform(
            post("/sprinkle")
            .header("X-USER-ID", "101112")
            .header("X-ROOM-ID", "klnmo")
            .content("{\"userCount\":15,\"money\":900}")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
        );

        // 대화방에 참여하지 않은 사용자가 뿌리기를 호출
        mockMvc.perform(
                post("/sprinkle")
                .header("X-USER-ID", "222324")
                .header("X-ROOM-ID", "klnmo")
                .content("{\"userCount\":3,\"money\":50}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        );

        // 돈을 0원으로 해서 뿌림
        mockMvc.perform(
                post("/sprinkle")
                        .header("X-USER-ID", "131415")
                        .header("X-ROOM-ID", "abcde")
                        .content("{\"userCount\":5,\"money\":0}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        );

        // 인원을 0명 으로 해서 뿌림
        mockMvc.perform(
                post("/sprinkle")
                        .header("X-USER-ID", "131415")
                        .header("X-ROOM-ID", "abcde")
                        .content("{\"userCount\":0,\"money\":700}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        );


        // 대화방 참여 인원 보다 뿌리기 인원을 많이 넣음
        mockMvc.perform(
                post("/sprinkle")
                        .header("X-USER-ID", "131415")
                        .header("X-ROOM-ID", "abcde")
                        .content("{\"userCount\":9,\"money\":700}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        );


        // 대화방 참여 인원 보다 뿌리기 인원을 많이 넣고 금액은 인원 만큼 넣음
        mockMvc.perform(
                post("/sprinkle")
                        .header("X-USER-ID", "131415")
                        .header("X-ROOM-ID", "abcde")
                        .content("{\"userCount\":9,\"money\":1}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        );
    }
}
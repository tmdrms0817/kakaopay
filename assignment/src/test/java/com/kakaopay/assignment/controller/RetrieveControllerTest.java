package com.kakaopay.assignment.controller;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class RetrieveControllerTest extends AbstractControllerTest{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ReceiveController receiveController;

    @Autowired
    private RetrieveController retrieveController;

    @Autowired
    private SprinkleController sprinkleController;

    @Override
    protected Object controller() {
        return receiveController;
    }

    @Test
    public void getRetrieve() throws Exception{

        mockMvc = MockMvcBuilders.standaloneSetup(sprinkleController)
                .addFilter(new CharacterEncodingFilter(StandardCharsets.UTF_8.name(), true))
                .alwaysDo(print())
                .build();

        // 뿌리기
        MvcResult result =  mockMvc.perform(
                post("/sprinkle")
                        .header("X-USER-ID", "101112")
                        .header("X-ROOM-ID", "klnmo")
                        .content("{\"userCount\":15,\"money\":45789}")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        JSONObject jsonObj =  new JSONObject(resultJson);
        JSONObject jObject2 = (JSONObject)jsonObj.get("data");
        String token = (String) jObject2.get("token");


        mockMvc = MockMvcBuilders.standaloneSetup(retrieveController)
                .addFilter(new CharacterEncodingFilter(StandardCharsets.UTF_8.name(), true))
                .alwaysDo(print())
                .build();
        // 조회
        mockMvc.perform(
                get("/retrieve")
                        .header("X-USER-ID", "161718")
                        .header("X-ROOM-ID", "klnmo")
                        .header("X-TOKEN", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)

        );


        mockMvc = MockMvcBuilders.standaloneSetup(receiveController)
                .addFilter(new CharacterEncodingFilter(StandardCharsets.UTF_8.name(), true))
                .alwaysDo(print())
                .build();


        // 받기 -- 성공
        mockMvc.perform(
                put("/receive")
                        .header("X-USER-ID", "161718")
                        .header("X-ROOM-ID", "klnmo")
                        .header("X-TOKEN", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)

        );


        mockMvc = MockMvcBuilders.standaloneSetup(retrieveController)
                .addFilter(new CharacterEncodingFilter(StandardCharsets.UTF_8.name(), true))
                .alwaysDo(print())
                .build();
        // 조회
        mockMvc.perform(
                get("/retrieve")
                        .header("X-USER-ID", "161718")
                        .header("X-ROOM-ID", "klnmo")
                        .header("X-TOKEN", token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)

        );
    }
}
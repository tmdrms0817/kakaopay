package com.kakaopay.assignment.util;

import com.kakaopay.assignment.dto.TokenDto;
import com.kakaopay.assignment.exception.BaseException;
import com.kakaopay.assignment.exception.RestCommonException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Date;

@Component
public class JwtTokenProvider{

    private String secretKey;
    private Duration expirationTime;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public JwtTokenProvider(
            @Value("${security.jwt.token.secret-key}") String secretKey,
            @Value("${security.jwt.token.expire-length}") Duration expirationTime) {
        this.secretKey = secretKey;
        this.expirationTime = expirationTime;
    }

    public String createToken(Date sprinkleTime, String token) {
        Date validity = new Date(System.currentTimeMillis() + (1000 * expirationTime.getSeconds()));

        return Jwts.builder()
                .claim("token", token)
                .setIssuedAt(sprinkleTime)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public TokenDto validateToken(String hashToken) throws BaseException{
        TokenDto tokenDto = new TokenDto();
        try{
            Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(hashToken).getBody();

            Date createDate = claims.get("iat",Date.class); // 시작일
            Date expiration = claims.get("exp",Date.class);  // 만료일
            String token = claims.get("token",String.class); // 토큰

            logger.debug("validateToken create = " +createDate.getTime());
            logger.debug("validateToken expiration = " +expiration.getTime());
            logger.debug("validateToken token = " +token);

            if(expiration.getTime() <= new Date().getTime())
                throw new RestCommonException("만료된 토큰 입니다.");

            tokenDto.setToken(token);
            tokenDto.setCreateDate(createDate);
            tokenDto.setExpiration(expiration);

        }catch(SignatureException e){
            throw new RestCommonException("JWT 토큰이 비정상입니다.");
        }catch (BaseException b){
            throw new RestCommonException(b.getMessage());
        }catch (Exception e){
            throw new RestCommonException(e.getMessage());
        }

        return tokenDto;
    }
}
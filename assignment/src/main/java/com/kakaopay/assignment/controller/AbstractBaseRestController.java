package com.kakaopay.assignment.controller;

import com.kakaopay.assignment.exception.BaseException;
import com.kakaopay.assignment.exception.InvalidRequestException;
import com.kakaopay.assignment.exception.UnknownException;
import com.kakaopay.assignment.massage.ResponseMessage;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.BindException;

@Slf4j
@RestController
public class AbstractBaseRestController{

    @ExceptionHandler(BaseException.class)
    public ResponseMessage abstractBaseException(HttpServletRequest req, HttpServletResponse res, final BaseException exception){
        log.error("BaseException : "+exception.getMessage());
        res.setStatus(exception.getHttpStatus().value());
        return new ResponseMessage(exception, req.getRequestURL().toString());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value= HttpStatus.BAD_REQUEST)
    public ResponseMessage illegalArgumentException(HttpServletRequest req, final IllegalArgumentException exception){
        log.error("IllegalArgumentException : "+exception.getMessage());
        return new ResponseMessage(new InvalidRequestException(exception.getMessage(), exception), req.getRequestURL().toString());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Internal server error")
    public ResponseMessage exception(HttpServletRequest req, final Exception exception){
        log.error("Exception : "+exception.getMessage());
        return new ResponseMessage(new UnknownException(exception.getMessage(), exception), req.getRequestURL().toString());
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(value=HttpStatus.NOT_FOUND, reason="not found")
    public ResponseMessage notFoundException(HttpServletRequest req, final Exception exception){
        log.error("Exception : "+exception.getMessage());
        return new ResponseMessage(new UnknownException(exception.getMessage(), exception), req.getRequestURL().toString());
    }
}


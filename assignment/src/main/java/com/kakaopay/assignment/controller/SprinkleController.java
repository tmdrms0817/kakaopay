package com.kakaopay.assignment.controller;


import com.kakaopay.assignment.config.Version;
import com.kakaopay.assignment.dto.SprinkleDto;
import com.kakaopay.assignment.massage.ResponseMessage;
import com.kakaopay.assignment.service.SprinkleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sprinkle")
@Version(1)
public class SprinkleController extends AbstractBaseRestController{

    @Autowired
    private SprinkleService sprinkleService;

    @PostMapping
    public ResponseMessage sprinkle(@RequestHeader(value = "X-USER-ID") int userId,
                                    @RequestHeader(value = "X-ROOM-ID") String roomId,
                                    @RequestBody SprinkleDto sprinkleDto){
        return sprinkleService.sprinkle(userId,roomId, sprinkleDto);
    }

}

package com.kakaopay.assignment.controller;

import com.kakaopay.assignment.config.Version;
import com.kakaopay.assignment.dto.TokenDto;
import com.kakaopay.assignment.massage.ResponseMessage;
import com.kakaopay.assignment.service.ReceiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/receive")
@Version(1)
public class ReceiveController extends AbstractBaseRestController{

    @Autowired
    private ReceiveService receiveService;

    @PutMapping
    public ResponseMessage receive(@RequestHeader(value = "X-USER-ID") int userId,
                                   @RequestHeader(value = "X-ROOM-ID") String roomId,
                                   @RequestHeader(value = "X-TOKEN") String token){
        return receiveService.receive(userId,roomId, token);
    }

}

package com.kakaopay.assignment.controller;

import com.kakaopay.assignment.config.Version;
import com.kakaopay.assignment.dto.TokenDto;
import com.kakaopay.assignment.massage.ResponseMessage;
import com.kakaopay.assignment.service.RetrieveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/retrieve")
@Version(1)
public class RetrieveController extends AbstractBaseRestController{

    @Autowired
    private RetrieveService retrieveService;

    @GetMapping
    public ResponseMessage receive(@RequestHeader(value = "X-USER-ID") int userId,
                                   @RequestHeader(value = "X-ROOM-ID") String roomId,
                                   @RequestHeader(value = "X-TOKEN") String token){
        return retrieveService.retrieve(userId,roomId,token);
    }
}

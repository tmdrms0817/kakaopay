package com.kakaopay.assignment.exception;

import org.springframework.http.HttpStatus;

public class RestCommonException extends BaseException{
    private static final long serialVersionUID = 1L;

    public RestCommonException() {
        super();
    }

    public RestCommonException(Throwable e) {
        super(e);
    }

    public RestCommonException(String errorMessge) {
        super(errorMessge);
    }

    public RestCommonException(String errorMessge, Throwable e) {
        super(errorMessge, e);
    }

    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}

package com.kakaopay.assignment.exception;

import org.springframework.http.HttpStatus;

public class UnknownException extends BaseException {

    private static final long serialVersionUID = 1L;

    public UnknownException() {
        super();
    }

    public UnknownException(Throwable e) {
        super(e);
    }

    public UnknownException(String errorMessge) {
        super(errorMessge);
    }

    public UnknownException(String errorMessge, Throwable e) {
        super(errorMessge, e);
    }

    public HttpStatus getHttpStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}

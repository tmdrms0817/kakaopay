package com.kakaopay.assignment.service;

import com.kakaopay.assignment.dto.SprinkleDto;
import com.kakaopay.assignment.dto.entity.PaySprinkleTb;
import com.kakaopay.assignment.exception.BaseException;
import com.kakaopay.assignment.exception.RestCommonException;
import com.kakaopay.assignment.massage.ResponseMessage;
import com.kakaopay.assignment.repository.PayRoomTbRepository;
import com.kakaopay.assignment.repository.SprinkleRepository;
import com.kakaopay.assignment.util.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class SprinkleService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    private PayRoomTbRepository payRoomTbRepository;

    @Autowired
    private SprinkleRepository sprinkleRepository;

    @Transactional
    public ResponseMessage sprinkle(int userId, String roomId, SprinkleDto sprinkleDto) throws BaseException{
        int userCount = sprinkleDto.getUserCount();
        int money = sprinkleDto.getMoney();
        int roomCount = payRoomTbRepository.countByRoomId(roomId);

        // 해당 대화방에 뿌리기를 하는 사용자가 있는지?
        int participationInChatRoom =  payRoomTbRepository.countByRoomIdAndUserId(roomId,userId);
        if( participationInChatRoom == 0 || participationInChatRoom > 1)
            throw new RestCommonException("뿌릴 인원이 해당 대화방에 참여하지 않음");

        // 파라미터 조건
        if(userCount < 1)
            throw new RestCommonException("뿌릴 인원은 1명 이상이여야 합니다.");

        if(money < 1)
            throw new RestCommonException("뿌릴 금액은 1원 이상이여야 합니다.");

        if(roomCount < 2)
            throw new RestCommonException("대화방에 인원이 2명 이하임.");

        // 최소 뿌리기 인원 보다 뿌리기로 받은 인원이 많으면 뿌리기 인원은 최소로 변경
        if((roomCount - 1) < userCount)
            userCount = roomCount - 1;

        // 뿌릴 금액보다 뿌릴 인원이 많으면 뿌리기 인원은 금액으로 변경
        if(money < userCount)
            userCount = money;

        // 금액 나누기
        int[] sprinkleMoneyArr = new int[userCount];

        if(userCount == 1){
            sprinkleMoneyArr[0] = money;
        }else{
            sprinkleMoneyArr = randomSprinkle(userCount,money);
        }

        logger.debug("randomSprinkle = "+Arrays.toString(sprinkleMoneyArr));

        // 토큰 만들기
        String token = createToken();

        // 입력 데이터 가공
        List<PaySprinkleTb> list = new ArrayList();
        Date sprinkleTime = new Date();

        for (int pay: sprinkleMoneyArr) {
            list.add(new PaySprinkleTb(token,pay,roomId,userId,0,sprinkleTime,null));
        }

        // 저장
        sprinkleRepository.saveAll(list);

        ResponseMessage message = new ResponseMessage(HttpStatus.OK);
        message.add("token",jwtTokenProvider.createToken(sprinkleTime,token));
        return message;
    }

    // 뿌리기 랜덤 분배
    private int[] randomSprinkle(int userCount , int money){
        int[] tmpArr = new int[userCount];
        int[] result = new int[userCount];
        Random random = new Random();
        int tmp = 0;

        for (int i =0; i<userCount; i++){
            if(money == tmp){
                for (int j =0; j<money; j++){
                    tmpArr[i+j] = 1;
                }
                break;
            }

            if(i == userCount-1){
                tmpArr[i] = money;
                break;
            }
            tmp = userCount - (i+1);
            money = money - tmp;
            tmpArr[i] =random.nextInt(money)+1;
            money = money - tmpArr[i] + tmp;
        }

        Arrays.sort(tmpArr);

        // 역정렬
        for(int i =0; i<tmpArr.length; i++){
            result[i] = tmpArr[(tmpArr.length-1) - i];
        }

        return result;
    }

    // 토큰 생성
    private String createToken() {
        StringBuffer buffer = new StringBuffer();
        Random random = new Random();
        String chars[] = "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".split(",");

        for (int i = 0; i < 3; i++) {
            buffer.append(chars[random.nextInt(chars.length)]);
        }
        return buffer.toString();
    }
}

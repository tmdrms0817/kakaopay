package com.kakaopay.assignment.service;

import com.kakaopay.assignment.dto.ReceiveListInterface;
import com.kakaopay.assignment.dto.ReceiveUserListDto;
import com.kakaopay.assignment.dto.RetrieveInterface;
import com.kakaopay.assignment.dto.TokenDto;
import com.kakaopay.assignment.exception.BaseException;
import com.kakaopay.assignment.exception.RestCommonException;
import com.kakaopay.assignment.massage.ResponseMessage;
import com.kakaopay.assignment.repository.PayRoomTbRepository;
import com.kakaopay.assignment.repository.SprinkleRepository;
import com.kakaopay.assignment.util.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RetrieveService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PayRoomTbRepository payRoomTbRepository;

    @Autowired
    private SprinkleRepository sprinkleRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Transactional
    public ResponseMessage retrieve(int userId, String roomId, String hashToken) throws BaseException{

        // 토큰 검증
        TokenDto tokenDto = jwtTokenProvider.validateToken(hashToken);
        logger.debug("===="+tokenDto);

        // 조회를 요청 인원 정보 확인
        int participationInChatRoom =  payRoomTbRepository.countByRoomIdAndUserId(roomId,userId);
        if( participationInChatRoom == 0 || participationInChatRoom > 1)
            throw new RestCommonException("조회를 요청한 인원 정보와 대화방 정보가 일치하지 않습니다.");

        // 시간 계산
        LocalDateTime tokenCreateLocalDateTime =  LocalDateTime.ofInstant(tokenDto.getCreateDate().toInstant(), ZoneId.systemDefault());
        Date plusTime = Date.from(tokenCreateLocalDateTime.plusSeconds(1).atZone(ZoneId.systemDefault()).toInstant());

        // 조회
        RetrieveInterface retrieveInterface = sprinkleRepository.findSumMoneyData(tokenDto.getToken(),userId,tokenDto.getCreateDate(),plusTime);
        List<ReceiveListInterface> list =  sprinkleRepository.findReceiveList(tokenDto.getToken(),userId,tokenDto.getCreateDate(),plusTime);

        List<ReceiveUserListDto> receiveUserListDtoList = new ArrayList();

        for (ReceiveListInterface receiveListInterface:list) {
            ReceiveUserListDto receiveUserListDto = new ReceiveUserListDto();
            receiveUserListDto.setMoney(receiveListInterface.getMoney());
            receiveUserListDto.setUserID(receiveListInterface.getReceiveUserId());
            receiveUserListDtoList.add(receiveUserListDto);
        }

        ResponseMessage message = new ResponseMessage(HttpStatus.OK);
        message.add("SprinkleTime",retrieveInterface.getSprinkleTime());
        message.add("SprinkleTotalMoney",retrieveInterface.getSprinkleTotalMoney());
        message.add("ReceiveTotalMoney",retrieveInterface.getReceiveTotalMoney());
        message.add("ReceiveUserList",receiveUserListDtoList);
        return message;
    }
}

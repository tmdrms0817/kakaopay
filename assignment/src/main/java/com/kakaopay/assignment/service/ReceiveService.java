package com.kakaopay.assignment.service;

import com.kakaopay.assignment.dto.TokenDto;
import com.kakaopay.assignment.dto.entity.PaySprinkleTb;
import com.kakaopay.assignment.exception.BaseException;
import com.kakaopay.assignment.exception.RestCommonException;
import com.kakaopay.assignment.massage.ResponseMessage;
import com.kakaopay.assignment.repository.PayRoomTbRepository;
import com.kakaopay.assignment.repository.SprinkleRepository;
import com.kakaopay.assignment.util.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
public class ReceiveService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PayRoomTbRepository payRoomTbRepository;

    @Autowired
    private SprinkleRepository sprinkleRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Transactional
    public ResponseMessage receive(int userId, String roomId, String hashToken) throws BaseException {

        // 토큰 검증
        TokenDto tokenDto = jwtTokenProvider.validateToken(hashToken);
        logger.debug("===="+tokenDto);

        // 해당 대화방에 받기를 하는 사용자가 있는지?
        int participationInChatRoom =  payRoomTbRepository.countByRoomIdAndUserId(roomId,userId);
        if( participationInChatRoom == 0 || participationInChatRoom > 1)
            throw new RestCommonException("받을 인원이 해당 대화방에 참여하지 않음");

        // 시간 계산
        LocalDateTime nowLocalTime = LocalDateTime.now();
        LocalDateTime tokenCreateLocalDateTime =  LocalDateTime.ofInstant(tokenDto.getCreateDate().toInstant(), ZoneId.systemDefault());
        Date plusTime = Date.from(tokenCreateLocalDateTime.plusSeconds(1).atZone(ZoneId.systemDefault()).toInstant());

        // 현재시간이 토큰이 생성된 시간에 10분을 더한거 보다 이전인지 확인
        if(!nowLocalTime.isBefore(tokenCreateLocalDateTime.plusMinutes(10)))
            throw new RestCommonException("10분이 지나서 더이상 받을수 없습니다.");

        // 한번이라도 받은적이 있는지 확인
        int count = sprinkleRepository.countByTokenAndRoomIdAndReceiveUserIdAndSprinkleTimeBetween(tokenDto.getToken(),roomId, userId, tokenDto.getCreateDate(),plusTime);
        if(count > 0)
            throw new RestCommonException("한번 받았으면 더이상 못받습니다.");

        // 토큰이,방번호,생성일이 같고 뿌리기건 중 받은 사람이 없는 건
        List<PaySprinkleTb> list= sprinkleRepository.findByTokenAndRoomIdAndReceiveUserIdAndSprinkleTimeBetweenOrderByIdxAsc(tokenDto.getToken(),roomId,0,tokenDto.getCreateDate(),plusTime);

        // 남은 뿌리기 건이 있는지 확인
        if(list.size() == 0)
            throw new RestCommonException("받을수 있는 뿌리기 건이 없습니다.");

        // 데이터 가공
        PaySprinkleTb upPaySprinkleTb = list.get(0);
        upPaySprinkleTb.setReceiveTime(new Date());
        upPaySprinkleTb.setReceiveUserId(userId);

        if(upPaySprinkleTb.getSprinkleUserId() == userId)
            throw new RestCommonException("자신이 뿌린 건은 받을수 없습니다.");

        // 저장 처리
        sprinkleRepository.save(upPaySprinkleTb);

        ResponseMessage message = new ResponseMessage(HttpStatus.OK);
        message.add("money",upPaySprinkleTb.getMoney());
        return message;
    }
}

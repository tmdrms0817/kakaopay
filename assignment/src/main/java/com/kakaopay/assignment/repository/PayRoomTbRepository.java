package com.kakaopay.assignment.repository;

import com.kakaopay.assignment.dto.entity.PayRoomTb;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PayRoomTbRepository extends JpaRepository<PayRoomTb, Long> {
    int countByRoomId(String roomId);
    int countByRoomIdAndUserId(String roomId, int userId);
}

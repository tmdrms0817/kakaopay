package com.kakaopay.assignment.repository;

import com.kakaopay.assignment.dto.ReceiveListInterface;
import com.kakaopay.assignment.dto.RetrieveInterface;
import com.kakaopay.assignment.dto.entity.PaySprinkleTb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface SprinkleRepository extends JpaRepository<PaySprinkleTb, Long> {

    // 받기 요청자가 한번이라도 받은적이 없는지 확인
    int countByTokenAndRoomIdAndReceiveUserIdAndSprinkleTimeBetween(String token,String roomId,int userId, Date createDate, Date createDatePlusOneSecond);

    // 받기 요청자가 받을수 있는 뿌리기 목록
    List<PaySprinkleTb> findByTokenAndRoomIdAndReceiveUserIdAndSprinkleTimeBetweenOrderByIdxAsc(String token,String roomId,int userId, Date createDate, Date createDatePlusOneSecond);

    // 뿌린시각 뿌린 금액, 받기 완료된 금액 조회
    @Query(value = "SELECT " +
                        "p.sprinkle_time AS sprinkleTime" +
                        ",IFNULL(SUM(p.money),0) AS sprinkleTotalMoney" +
                        ",( select " +
                                "IFNULL(SUM(p1.money),0) as receiveTotalMoney " +
                            "from PAY_SPRINKLE_TB p1 " +
                            "where p1.token = ?1 " +
                                "AND p1.sprinkle_user_id = ?2 " +
                                "AND p1.receive_user_id != 0 " +
                                "AND (p1.sprinkle_time between ?3 and ?4)" +
                        ") AS receiveTotalMoney " +
                    "FROM PAY_SPRINKLE_TB p " +
                    "WHERE p.token = ?1 " +
                        "AND p.sprinkle_user_id = ?2 " +
                        "AND (p.sprinkle_time BETWEEN ?3 AND ?4)"
            ,nativeQuery = true)
    RetrieveInterface findSumMoneyData(String token, int userId, Date createDate, Date createDatePlusOneSecond);

    // 받은 금액, 받은 사용자 아이디 목록 조회
    @Query(value = "select p.money, p.receive_user_id as receiveUserId from PAY_SPRINKLE_TB p where token = ?1 AND sprinkle_user_id = ?2 AND receive_user_id != 0 and (p.sprinkle_time between ?3 and ?4)",nativeQuery = true)
    List<ReceiveListInterface> findReceiveList(String token, int userId, Date createDate, Date createDatePlusOneSecond);
}

package com.kakaopay.assignment.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class TokenDto {
    private String token;
    private Date createDate;
    private Date expiration;
}

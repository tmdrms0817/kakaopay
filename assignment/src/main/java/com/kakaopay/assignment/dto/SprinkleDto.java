package com.kakaopay.assignment.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SprinkleDto {
    public int userCount;
    public int money;
}

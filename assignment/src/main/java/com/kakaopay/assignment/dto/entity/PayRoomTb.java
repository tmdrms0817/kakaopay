package com.kakaopay.assignment.dto.entity;

import lombok.*;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name="payRoomTb")
public class PayRoomTb {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;
    private String roomId;
    private int userId;

    @Builder
    public PayRoomTb(String roomId, int userId) {
        this.roomId = roomId;
        this.userId = userId;
    }
}

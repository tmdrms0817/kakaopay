package com.kakaopay.assignment.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReceiveUserListDto {
    private int userID;
    private int money;
}

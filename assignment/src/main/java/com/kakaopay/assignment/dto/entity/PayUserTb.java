package com.kakaopay.assignment.dto.entity;

import lombok.*;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name="payUserTb")
public class PayUserTb {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;
    private int userId;

    @Builder
    public PayUserTb(int userId) {
        this.userId = userId;
    }

}

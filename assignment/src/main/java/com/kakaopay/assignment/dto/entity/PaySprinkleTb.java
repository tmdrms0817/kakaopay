package com.kakaopay.assignment.dto.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name="paySprinkleTb")
public class PaySprinkleTb {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;
    private String token;
    private int money;
    private String roomId;
    private int sprinkleUserId;
    private int receiveUserId;


    private Date sprinkleTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date receiveTime;

    @Builder
    public PaySprinkleTb(String token, int money, String roomId, int sprinkleUserId, int receiveUserId, Date sprinkleTime, Date receiveTime) {
        this.token = token;
        this.money = money;
        this.roomId = roomId;
        this.sprinkleUserId = sprinkleUserId;
        this.receiveUserId = receiveUserId;
        this.sprinkleTime = sprinkleTime;
        this.receiveTime = receiveTime;
    }
}

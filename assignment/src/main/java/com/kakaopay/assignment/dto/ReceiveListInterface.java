package com.kakaopay.assignment.dto;

public interface ReceiveListInterface {
    Integer getMoney();
    Integer getReceiveUserId();
}

package com.kakaopay.assignment.dto;

import java.util.Date;

public interface RetrieveInterface {
    Date getSprinkleTime();
    Integer getSprinkleTotalMoney();
    Integer getReceiveTotalMoney();
}
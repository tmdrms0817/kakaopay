package com.kakaopay.assignment.massage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorMessage {

    private int code;
    private String errorMessage;
    private String referedUrl;

    public ErrorMessage(int code, String errorMessage, String referedUrl) {
        this.code = code;
        this.errorMessage = errorMessage;
        this.referedUrl = referedUrl;
    }
}